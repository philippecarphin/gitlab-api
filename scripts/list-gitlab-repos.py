#!/usr/bin/env python3
import argparse
import requests
import sys
from pprint import pprint

def main():
    p = argparse.ArgumentParser()
    p.add_argument("--username", default="philippecarphin", help="Gitlab username")
    args = p.parse_args()
    resp = search_user_by_username(args.username)

    user_repos = list_user_repos(args.username)
    for r in sorted(user_repos, key=lambda r:r['name']):
        print(f"{r['id']} {r['name']:<25} {r['ssh_url_to_repo']}")

def list_user_repos(username):
    user_id = search_user_by_username(username)[0]['id']
    resp = requests.get(f"https://gitlab.com/api/v4/users/{user_id}/projects")

    user_repos = resp.json()

    ids = [r['id'] for r in user_repos]
    max_id = max(ids)
    min_id = min(ids)

    user_id = search_user_by_username(username)[0]['id']
    user_repos += requests.get(f"https://gitlab.com/api/v4/users/{user_id}/projects", params={"id_after": max_id}).json()
    user_repos += requests.get(f"https://gitlab.com/api/v4/users/{user_id}/projects", params={"id_before": min_id}).json()

    return user_repos



def search_user_by_username(username):
    username = "philippecarphin"
    resp = requests.get(f"https://gitlab.com/api/v4/users?username={username}")
    return resp.json()

if __name__ == "__main__":
    main()
