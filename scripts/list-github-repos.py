#!/usr/bin/env python3
import json
import requests
resp = requests.get("https://api.github.com/users/philippecarphin/repos?page=1&per_page=100").json()
resp += requests.get("https://api.github.com/users/philippecarphin/repos?page=2&per_page=100").json()
repos = resp

for repo in sorted(repos, key=lambda r:r['name']):
    print("{id:<14} {name:<34} {ssh_url}".format(**repo))
