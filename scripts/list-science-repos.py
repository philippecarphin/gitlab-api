#!/usr/bin/env python3
import argparse
import os
import requests
import sys
from pprint import pprint

with open(os.environ["HOME"] + "/.ssh/gitlab-access-token", 'r') as f:
    token = f.read()
headers = {"PRIVATE-TOKEN":token}
def main():
    p = argparse.ArgumentParser()
    p.add_argument("--username", default="phc001", help="Gitlab username")
    args = p.parse_args()
    # resp = search_user_by_username(args.username)

    my_repos, other_repos = list_user_repos(args.username)
    # print(f"\033[1;34mContributed repos\033[0m")
    # for r in sorted(other_repos, key=lambda r:r['name']):
    #     print(f"{r['id']} {r['name']:<35} {r['ssh_url_to_repo']}")

    print(f"\033[1;34mPersonal repos\033[0m")
    for r in sorted(my_repos, key=lambda r:r['name']):
        print(f"{r['id']} {r['name']:<35} {r['ssh_url_to_repo']}")
    # pprint(user_repos)

def list_user_repos(username):
    user_result = search_user_by_username(username)
    user_id = user_result[0]['id']
    resp = requests.get(f"https://gitlab.science.gc.ca/api/v3/projects?per_page=100&page=1",headers=headers)
    user_repos = resp.json()
    resp = requests.get(f"https://gitlab.science.gc.ca/api/v3/projects?per_page=100&page=2",headers=headers)
    user_repos += resp.json()


    my_repos = [r for r in user_repos if r['namespace']['owner_id'] == 513]
    other_repos = [r for r in user_repos if r['namespace']['owner_id'] != 513]

    return my_repos, other_repos


def search_user_by_username(username):
    resp = requests.get(f"https://gitlab.science.gc.ca/api/v3/users?username={username}", headers={"PRIVATE-TOKEN": token.strip()})
    return resp.json()

if __name__ == "__main__":
    main()
