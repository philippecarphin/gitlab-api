#!/usr/bin/env python3

lines = []
with open("com.txt", 'r') as f:
    lines += f.read().splitlines()
with open("science.txt", 'r') as f:
    lines += f.read().splitlines()
with open("hub.txt", 'r') as f:
    lines += f.read().splitlines()

repos = []
for l in lines:
    words = l.split()

    repos.append({
        "name": words[0],
        "url": words[1]
    })

for r in sorted(repos, key=lambda e : e['name']):
    print("{name:<34} {url}".format(**r))


